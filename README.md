# drupalsite-extraconfig-template

This project is a template for users to develop their extra-configuration composers for Drupal Websites hosted on drupal.cern.ch .


To use composer locally and reliabily, you can develop from our Docker image:
Image: gitlab-registry.cern.ch/drupal/paas/cern-drupal-distribution/composer-builder:master-RELEASE-2021.10.07T14-52-06Z

Note: to fully use this feature as intended, please follow all the steps
described
[here](https://drupal-community.web.cern.ch/t/creating-a-drupal-site-with-extra-modules-under-construction/1123#h-6-adding-webhook-triggers-to-your-gitlab-repo-in-development-7)

New change
